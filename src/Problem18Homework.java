import java.util.Scanner;

import javax.sql.rowset.spi.SyncResolver;

public class Problem18Homework {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        int type;
        int num;
        

        while (true) {
            System.out.print("Please serect star type [ 1-4 , 5 is Exit]: ");
            type = sc.nextInt();
            if (type == 1) {
                System.out.print("Please input number: ");
                num = sc2.nextInt();
                for (int i=0; i<num; i++) {
                    for (int j=0; j<i+1; j++) {
                        System.out.print('*');
                    }
                    System.out.println();
                }
            } else if (type == 2) {
                System.out.print("Please input number: ");
                num = sc2.nextInt();
                for (int i=num; i>0; i--) {
                    for (int j=0; j<i; j++) {
                        System.out.print('*');
                    }
                    System.out.println();
                }
            } else if (type == 3) {
                System.out.print("Please input number: ");
                num = sc2.nextInt();
                for (int i=0; i<num; i++) {
                    for (int j=0; j<num; j++) {
                        if (j>=i) {
                            System.out.print('*');
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            } else if (type == 4) {
                System.out.print("Please input number: ");
                num = sc2.nextInt();
                for (int i=4; i>=0; i--) {
                    for (int j=0; j<num; j++) {
                        if (j>=i) {
                            System.out.print('*');
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            } else if (type == 5) {
                System.out.print("Bye bye!!!");
                break;
            } else {
                System.out.println("Error: Please input number between 1-5");
            }
        }
    }
}
