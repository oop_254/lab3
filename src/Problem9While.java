import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        System.out.print("Please input n: ");
        num = sc.nextInt();

        int i=1;
        while (i <= num) {
            int j = 1;
            while (j <= num) {
                System.out.print(j);
                j ++ ;
            }
            System.out.println();
            i ++ ;
        }

    }
}
